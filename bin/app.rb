require 'aws-sdk-s3'
require 'twitter'
require 'open-uri'

ACCEPTED_FORMATS = ['.gif', '.jpg', '.png']

# Connect to Twitter.
twitter = Twitter::REST::Client.new do |config|
  config.consumer_key = ENV['TWITTER_CONSUMER_KEY']
  config.consumer_secret = ENV['TWITTER_CONSUMER_SECRET']
  config.access_token = ENV['TWITTER_ACCESS_TOKEN']
  config.access_token_secret = ENV['TWITTER_ACCESS_TOKEN_SECRET']
end

# Connect to Amazon S3.
Aws.config.update({
  region: 'us-west-1',
  credentials: Aws::Credentials.new(ENV['S3_ACCESS_KEY'],
    ENV['S3_SECRET_ACCESS_KEY'])
})

# Grab the bucket where our images are stored.
bucket = Aws::S3::Resource.new.bucket(ENV['S3_BUCKET'])

# Tweet any image that has not been tapped.
bucket.objects.each do |item|
  key = item.key.downcase
  if !key.end_with?('_tapped') && key.end_with?(*ACCEPTED_FORMATS)
    map_name = ''
    begin
      map_name = open(bucket.object("#{key}.txt").presigned_url(:get)).read
    rescue
      # Don't care so keep working.
    end
    twitter.update_with_media(map_name, open(item.presigned_url(:get)))
    # Rename image so it's not ever tweeted again.
    item.move_to("#{ENV['S3_BUCKET']}/#{key}_tapped")
    break
  end
end
